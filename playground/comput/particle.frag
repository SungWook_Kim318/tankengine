#version 330
in vec2 txcoord;
layout(location = 0) out vec4 FragColor;

void main() 
{
  //Attenuation to outside
  float s = (1/(1+15.*dot(txcoord, txcoord))-1/16.);
  FragColor = s*vec4(1.0,1.0,1.0,1);
}