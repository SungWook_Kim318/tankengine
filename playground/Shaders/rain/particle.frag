#version 430
in vec2 txcoord;

layout(location = 0) out vec4 FragColor;

uniform float radius = 0.5f;


uniform sampler2D t0;
void main() 
{
  //Attenuation to outside
  /*
  float s = (1/(1+15.*dot(txcoord, txcoord))-1/8.);
  if(s <= radius)
    discard;
  FragColor = vec4(s);
  */
  vec4 color = texture2D(t0, txcoord);
  float s = color.a;
  if( s < 0.001f)
    discard;
  
  FragColor = color;
}