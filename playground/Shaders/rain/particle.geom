#version 430
layout(location = 0) uniform mat4 View;
layout(location = 1) uniform mat4 Projection;
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;
out vec2 txcoord;

/*
in VS_OUT
{
  vec4 velocity;
} gs_in[];
*/
uniform vec4 velocity;
uniform vec2 particleSize;

void main() 
{
  vec4 pos = View*gl_in[0].gl_Position;
  vec4 verPos;
  vec2 newSize;
  vec2 origin;

  // bottom left
  txcoord = vec2( 0, 0); 
  origin  = vec2(-1,-1);
  newSize = particleSize * origin;
  verPos = pos + vec4(newSize,0,0);
  verPos -= vec4(particleSize,0,0) * velocity;
  gl_Position = Projection*verPos;
  EmitVertex();

  // bottom right
  txcoord = vec2( 1, 0);
  origin  = vec2( 1,-1);
  newSize = particleSize * origin;
  verPos = pos + vec4(newSize,0,0);
  verPos -= vec4(particleSize,0,0) * velocity;
  gl_Position = Projection*verPos;
  EmitVertex();

  
  // top left
  txcoord = vec2( 0, 1);
  origin  = vec2(-1, 1);
  newSize = particleSize * origin;
  verPos = pos + vec4(newSize,0,0);
  verPos += vec4(particleSize,0,0) * velocity;
  gl_Position = Projection*verPos;
  EmitVertex();

  // top right
  txcoord = vec2( 1, 1);
  origin  = vec2( 1, 1);
  newSize = particleSize * origin;
  verPos = pos + vec4(newSize,0,0);
  verPos += vec4(particleSize,0,0) * velocity;
  gl_Position = Projection*verPos;
  EmitVertex();

  EndPrimitive();
}