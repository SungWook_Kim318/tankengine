#version 440 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;

out vec3 Position;
out vec3 Normal;
out vec2 TexCoords;
out vec3 Color;

uniform mat4 ModelView; //o
uniform mat4 MVP; //o
uniform mat3 NormalMatrix; //o 

uniform vec4 LightPosition; // view*lightPosition
uniform vec3 LightIntensity; //(1,1,1)
uniform vec3 Kd; // (0.8, 0.8, 0.8)
uniform vec3 Ka; // (0.3, 0.3, 0.3)
uniform vec3 Ks; // (0.3, 0.3, 0.3)
uniform float shininess; // 10.f
uniform vec3 camPos; //eye

vec3 ads(vec4 position, vec3 norm)
{
   vec3 s = normalize(vec3(LightPosition-position));

   float l = length(position);
   vec3 v = normalize(vec3(-position));
   vec3 r = reflect(-s,norm);


   vec3 specular = vec3(0, 0, 0);
   if (dot(s, norm) > 0.0) {
      specular = LightIntensity * Ks * pow(max(dot(r, v), 0.0),max(50.0, shininess));
   }
  // return LightIntensity * (Ka + Kd * max(dot(s, norm), 0.0));

   return LightIntensity * (Ka + Kd * max(dot(s,norm),0.0)) + LightIntensity * Ks * pow(max(dot(r, v), 0.0), shininess);
}

void main()
{
  float l = length(position);
  vec3 v = normalize(vec3(-position));
  vec3 eyeNorm = normalize(NormalMatrix * normal);
  vec4 eyePosition = ModelView * vec4(position,1.0);

  Color = ads(eyePosition,eyeNorm);
  TexCoords = texCoords;

  gl_Position = MVP * vec4(position, 1.0f);
  Normal = eyeNorm;
  Position = vec3(eyePosition.xyz);
}