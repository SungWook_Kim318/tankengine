#version 430
in vec2 txcoord;
layout(location = 0) out vec4 FragColor;
uniform float radius = 0.5f;
void main() 
{
  //Attenuation to outside
  float s = (1/(1+15.*dot(txcoord, txcoord))-1/8.);
  if(s <= radius)
    discard;
  FragColor = s*vec4(1.0);
}