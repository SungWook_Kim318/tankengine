#version 430
layout(location = 0) uniform mat4 View;
layout(location = 1) uniform mat4 Projection;
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;
out vec2 txcoord;

#define size 1.0
void main() 
{
  vec4 pos = View*gl_in[0].gl_Position;
  vec4 ratio = vec4(1.0,1.0,0,0);
  txcoord = vec2(-1,-1);
  gl_Position = Projection*(pos+vec4(txcoord,0,0));
  EmitVertex();
  txcoord = vec2( 1,-1);
  gl_Position = Projection*(pos+vec4(txcoord,0,0));
  EmitVertex();
  txcoord = vec2(-1, 1);
  gl_Position = Projection*(pos+vec4(txcoord,0,0));
  EmitVertex();
  txcoord = vec2( 1, 1);
  gl_Position = Projection*(pos+vec4(txcoord,0,0));
  EmitVertex();
}