#version 450 core

#define saturate(x) clamp(x, 0.0, 1.0)
#define kPi 3.1415926535897

in vec2 TexCoords;

out vec4 color;

uniform sampler2D screenTexture;
uniform sampler2D screenTexture2;
uniform float time;
uniform vec2 resolution; // unused
uniform mat4 projection; // unused
uniform mat4 view;


uniform vec3 camPos;   

//#define LIGHT_SNOW // Comment this out for a blizzard

#ifdef LIGHT_SNOW
  #define LAYERS 50
  #define DEPTH .5  //was 0.5
  #define WIDTH  0.3
  #define SPEED .6
#else // BLIZZARD
  #define LAYERS 200
  #define DEPTH .1
  #define WIDTH .8
  #define SPEED 1.5
#endif


vec3 LinearToSrgb(vec3 rgb)
{
  return pow(rgb, vec3(1.0 / 2.2));
}

vec3 SrgbToLinear(vec3 rgb)
{
  return pow(rgb, vec3(2.2));
}

float LinearToSrgb(float rgb)
{
  return pow(rgb, float(1.0/2.2));
}

float SrgbToLinear(float rgb)
{
  return pow(rgb, float(2.2));
}

vec3 ToneMap(vec3 rgb)
{
  // simple Reinhard like tone mapping
  float l = dot(rgb, vec3(0.212671, 0.715160, 0.072169));

  return rgb / (l + 1.0);
}


float CalculateShadowFactor(sampler2DShadow texDepth, vec3 uvw, const int samples)
{
  // Poisson disc
  vec2 vTaps[12] = vec2[](vec2(-0.326212,-0.40581),vec2(-0.840144,-0.07358),
                          vec2(-0.695914,0.457137),vec2(-0.203345,0.620716),
                          vec2(0.96234,-0.194983),vec2(0.473434,-0.480026),
                          vec2(0.519456,0.767022),vec2(0.185461,-0.893124),
                          vec2(0.507431,0.064425),vec2(0.89642,0.412458),
                          vec2(-0.32194,-0.932615),vec2(-0.791559,-0.59771));

  float s = 0.0;
  float radius = 0.002;

  for (int i=0; i < 12; i++)
  {
  //	s += sampler2DShadow (texDepth, vec3(uvw.xy + vTaps[i]*radius, uvw.z)).r;

  }

  s /= samples;

  return s;
}

void SolveQuadratic(float a, float b, float c, out float minT, out float maxT)
{
  float discriminant = b*b - 4.0*a*c;

  if (discriminant < 0.0)
  {
    // no real solutions so return a degenerate result
    maxT = 0.0;
    minT = 0.0;
    return;
  }

  // numerical receipes 5.6 (this method ensures numerical accuracy is preserved)
  float t = -0.5 * (b + sign(b)*sqrt(discriminant));
  float closestT = t / a;
  float furthestT = c / t;

  if (closestT > furthestT)
  {	
    minT = furthestT;
    maxT = closestT;
  }
  else
  {
    minT = closestT;
    maxT = furthestT;
  }
}

float MiniMax3(float x)
{
   return ((-0.130234*x - 0.0954105)*x + 1.00712)*x - 0.00001203333;
}
 
float atanMiniMax3(float x)
{
   // range reduction
   if (x < 1)
      return MiniMax3(x);
   else
      return kPi*0.5 - MiniMax3(1.0/x);
}

// p : ray starting point
// dir : ray direction
// lightpos : light position
// d : distance traveled computed by entry /exit point of ray with the volume
float InScatter(vec3 p, vec3 dir, vec3 lightPos, float d)
{

  // calculate quadratic coefficients a,b,c
  vec3 q = p - lightPos; 
   
  float b = dot(dir, q);
  float c = dot(q, q);

  // evaluate integral
  float s = 1.0f / sqrt(c - b*b);

  float l = s * (atan( (d + b) * s) - atan( b*s ));

  return l;	
}



// simple diffuse + blinn phong lighting model 
void Li(vec3 surfacePos, vec3 surfaceNormal, vec3 lightPos, vec3 eyePos, float specularPower, out float diffuse, out float specular)
{
  vec3 l = lightPos-surfacePos;
  vec3 e = normalize(eyePos-surfacePos);

  float d = length(l);
  l /= d;

  // half vector
  vec3 h = normalize(e+l);
  
  diffuse = saturate(dot(surfaceNormal, l)) / (d*d);
  specular = pow(saturate(dot(h,surfaceNormal)), specularPower);
}


float LinearizeDepth(in vec2 uv)
{
  float zNear = 0.1;    // TODO: Replace by the zNear of your perspective projection
  float zFar  = 500.0; // TODO: Replace by the zFar  of your perspective projection
  float depth = texture(screenTexture2, uv).x;
  return (2.0 * zNear) / (zFar + zNear - depth * (zFar - zNear));
}

// Fract = x - floor(x) => [0, 1]
float rand2D(in vec2 co){
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

float rand3D(in vec3 co){
  return fract(sin(dot(co.xyz ,vec3(12.9898,78.233,144.7272))) * 43758.5453);
}

float simple_interpolate(in float a, in float b, in float x)
{
  return a + smoothstep(0.0,1.0,x) * (b-a);
}

float interpolatedNoise3D(in float x, in float y, in float z)
{
  float integer_x = x - fract(x);
  float fractional_x = x - integer_x;

  float integer_y = y - fract(y);
  float fractional_y = y - integer_y;

  float integer_z = z - fract(z);
  float fractional_z = z - integer_z;

  float v1 = rand3D(vec3(integer_x, integer_y, integer_z));
  float v2 = rand3D(vec3(integer_x+1.0, integer_y, integer_z));
  float v3 = rand3D(vec3(integer_x, integer_y+1.0, integer_z));
  float v4 = rand3D(vec3(integer_x+1.0, integer_y +1.0, integer_z));

  float v5 = rand3D(vec3(integer_x, integer_y, integer_z+1.0));
  float v6 = rand3D(vec3(integer_x+1.0, integer_y, integer_z+1.0));
  float v7 = rand3D(vec3(integer_x, integer_y+1.0, integer_z+1.0));
  float v8 = rand3D(vec3(integer_x+1.0, integer_y +1.0, integer_z+1.0));

  float i1 = simple_interpolate(v1,v5, fractional_z);
  float i2 = simple_interpolate(v2,v6, fractional_z);
  float i3 = simple_interpolate(v3,v7, fractional_z);
  float i4 = simple_interpolate(v4,v8, fractional_z);

  float ii1 = simple_interpolate(i1,i2,fractional_x);
  float ii2 = simple_interpolate(i3,i4,fractional_x);

  return simple_interpolate(ii1 , ii2 , fractional_y);
}

float Noise3D(in vec3 coord, in float wavelength)
{
  return interpolatedNoise3D(coord.x/wavelength, coord.y/wavelength, coord.z/wavelength);
}


float dotNoise2D(in float x, in float y, in float fractionalMaxDotSize, in float dDensity)
{
  float integer_x = x - fract(x);
  float fractional_x = x - integer_x;

  float integer_y = y - fract(y);
  float fractional_y = y - integer_y;

  if (rand2D(vec2(integer_x+1.0, integer_y +1.0)) > dDensity)
  {
    return 0.0;
  }

  float xoffset = (rand2D(vec2(integer_x, integer_y)) -0.5);
  float yoffset = (rand2D(vec2(integer_x+1.0, integer_y)) - 0.5);
  float dotSize = 0.5 * fractionalMaxDotSize * max(0.25,rand2D(vec2(integer_x, integer_y+1.0)));

  vec2 truePos = vec2 (0.5 + xoffset * (1.0 - 2.0 * dotSize) , 0.5 + yoffset * (1.0 -2.0 * dotSize));

  float distance = length(truePos - vec2(fractional_x, fractional_y));

  return 1.0 - smoothstep (0.3 * dotSize, 1.0* dotSize, distance);

}

float DotNoise2D(in vec2 coord, in float wavelength, in float fractionalMaxDotSize, in float dDensity)
{
  return dotNoise2D(coord.x/wavelength, coord.y/wavelength, fractionalMaxDotSize, dDensity);
}


void main()
{
  float snow = 0.0;
  float gradient =   (1.0- (gl_FragCoord.x / resolution.x)) * 0.4f;
  float random = fract(sin(dot(gl_FragCoord.xy,vec2(12.9898,78.233)))*43758.5453);

  const mat3 p = mat3(13.323122,23.5112,21.71123,
                      21.1212,28.7312,11.9312,
                      21.8112,14.7212,61.3934);


  vec3 acc = vec3(0.7);
  float dof = 5.0 * sin(time*0.1f);
  float size = 1.0f;

  float w;

  float depth = 0;

  float c = LinearizeDepth(vec2(gl_FragCoord.x/resolution.x,gl_FragCoord.y/resolution.y));

  vec2 uv= TexCoords;

  float t = time;
  float f= 5.*sin(t*.1);
  bool flag = false;
  vec2 q;
  float ly = 100.0f;

   w = 0.5;  //object : 1,  snow : 0

  /**** Heavy Part ****/
  for (float i=0.; i<ly; i++) 
  {
    float j = i*.1;

    q = uv* (1.+j);
    float xoffset =  q.y* .8 * ( fract(i*7.238917) - .5 );
    float yoffset =  t / (1.+j*.03);
    q += vec2( xoffset,yoffset);

    vec3 m = vec3( ceil(q), 31. )/ 100000.0f;

    vec3 r = fract( (31415.9+m) / fract(p*m) );
    vec2 s = abs( fract(q)  -.5 + .9*r.xy-.45 )  + .02*abs( fract(10.*q.xy) - .5); 


    float div =  (.6* (s.x+s.y) + max(s.x,s.y) -.01 )  / ( .005+ .05* min( .5* abs(i-5.-f), 1.) );

    acc += smoothstep(1.,-1., div) * r.x / (1+0.02*j);

    depth = (i/ly*0.3);

    if (depth > c) 
    {
      break;
    }
  }
  /**** Draw Snow Flake on Frame ****/
  if (c < 1)
    color = vec4(mix(texture(screenTexture, TexCoords.st).rgb,acc,w),1.0);
    //color = vec4(mix(texture(screenTexture, TexCoords.st).rgb,acc,w),1.0);
    //color = vec4(texture(screenTexture, TexCoords.st).rgb,1.0);
  else 
    color = vec4(acc,1.0);

//	if (c > 0) color = vec4(1,0,0,1);
    
//	color = vec4(mix(texture(screenTexture, TexCoords.st).rgb,acc,0),1.0);
//  color = vec4(c,c,c,1.0);


} 
