#version 330 core


layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texCoords;

out vec2 TexCoords;
out vec4 posPos;

void main()
{

	float FXAA_SUBPIX_SHIFT = 1.0/4.0;
	float rt_w = 720.0; // GeeXLab built-in
	float rt_h = 750.0f; // GeeXLab built-in

	
	vec2 rcpFrame = vec2(1.0/rt_w, 1.0/rt_h);
	posPos.xy = texCoords.xy;
	posPos.zw = texCoords.xy - 
                  (rcpFrame * (0.5 + FXAA_SUBPIX_SHIFT));


    gl_Position = vec4(position.x, position.y, 0.0f, 1.0f); 
    TexCoords = texCoords;
}  