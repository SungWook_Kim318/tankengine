#version 330 core
in vec2 TexCoords;

out vec4 color;

uniform sampler2D screenTexture;

const float offset = 1.0 / 300;  




void main()
{
	
	float vx_offset = 0.5f;
	float pixel_w = 5.0f;
    float pixel_h = 5.0f;
	float rt_w = 780; 
    float rt_h = 750;

	vec2 uv = TexCoords.st;
    vec3 tc = vec3(1.0, 0.0, 0.0);
    if (uv.x < (vx_offset-0.005))
	{
		float dx = pixel_w*(1./rt_w);
		float dy = pixel_h*(1./rt_h);
		vec2 coord = vec2(dx*floor(uv.x/dx),dy*floor(uv.y/dy));
		tc = texture(screenTexture, coord).rgb;
    }
	else if (uv.x>=(vx_offset+0.005))
	{
		tc = texture(screenTexture, uv).rgb;
    }
	color = vec4(tc, 1.0);
	
} 
