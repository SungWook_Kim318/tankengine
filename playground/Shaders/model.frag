
#version 440 core

in vec2 TexCoords;
in vec3 Color;

in vec3 Position;
in vec3 Normal;

uniform sampler2D texture_diffuse1; // NONE
uniform sampler2D texture_specular1; // NONE

uniform int hasTextureDiffuse; // false
uniform int hasTextureSpecular; // false 

out vec4 color;

struct SpotLightInfo {
    vec4 position;   // Position in eye coords
    vec3 intensity;
    vec3 direction;  // Direction of the spotlight in eye coords.
    float exponent;  // Angular attenuation exponent
    float cutoff;    // Cutoff angle (between 0 and 90)
    float innerCutoff;    // Cutoff angle (between 0 and 90)
};
uniform SpotLightInfo Spot;

uniform vec3 Kd;            // Diffuse reflectivity
uniform vec3 Ka;            // Ambient reflectivity
uniform vec3 Ks;            // Specular reflectivity
uniform float shininess;    // Specular shininess factor


uniform float constantAttenuation = 1.0f;
uniform float linearAttenuation = 0.01f;
uniform float quadraticAttenuation = 0.001f;


vec3 calculateSpot()
{
    vec3 L = Spot.position.xyz - Position;
    float D = length(L);
    L = normalize(L);
    float attenuation = 1.0f / (constantAttenuation +
                                linearAttenuation * D +
                                quadraticAttenuation * D * D );
    float spotDot = dot(-L, normalize(Spot.direction));
    float angle = acos(spotDot);

    float spotAttenuation;
    if (angle < radians(Spot.innerCutoff))
    {
        //inside the inner cone
        spotAttenuation = 1.0f;
    }
    else {
      float v  = smoothstep(cos(radians(Spot.cutoff)),cos(radians(Spot.innerCutoff)),spotDot);
      //outside of the inner cone
      spotAttenuation = pow(v,Spot.exponent);
    }
    attenuation *= spotAttenuation;
    //blinn - phong
    vec3 V = normalize(-Position);
    vec3 H = normalize(V + L);

    float dotNL = max(0, dot(L,Normal));
    float dotHN = max(0, dot(H,Normal));
    float pf = pow(dotHN,shininess);

    vec3 ambient = Ka * Spot.intensity * attenuation;
    vec3 diffuse = Kd * Spot.intensity * dotNL * attenuation;
    vec3 specular= Ks * Spot.intensity * pf * attenuation;

    vec3 color = ambient + diffuse + specular;
    return color;
}

void main()
{    
  color = vec4(calculateSpot(), 1.0f);
  //color.rgb = max(color.rgb, vec3(0.0f));
  color.rgb += Color;
  /*
  if (hasTextureDiffuse == 1) 
  {
    vec4 diffuse = texture(texture_diffuse1, TexCoords);
    color.rgb += Color * diffuse.xyz;
  }
  else
  {// operate that.
    color.rgb += Color;
  }

  if (hasTextureSpecular == 1) 
  {
    vec4 specular = texture(texture_specular1, TexCoords).rgba;
    color += specular;
  }
  */
}

