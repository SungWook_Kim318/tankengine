﻿#define GLM_ENABLE_EXPERIMENTAL 
#include <iostream>
#include "MyGlWindow.hpp"
#include <vector>

#include  <glm/gtx/string_cast.hpp>
#include <glm/gtc/type_ptr.hpp>


#include "global.hpp"
#include "Engine/TimeManager.hpp"

#include <Externs/IMGUI/imgui.h>
#include <Externs/IMGUI/imgui_impl_glfw.h>
#include <Externs/IMGUI/imgui_impl_opengl3.h>

static float DEFAULT_VIEW_POINT[3] = {20, 20, 20};
static float DEFAULT_VIEW_CENTER[3] = {0, 0, 0};
static float DEFAULT_UP_VECTOR[3] = {0, 1, 0};




MyGlWindow::MyGlWindow(int w, int h)
  : m_snow{}
//==========================================================================
{

  m_width = w;
  m_height = h;

  z_far = 500.f;
  z_near = 0.1f;

  glm::vec3 viewPoint(DEFAULT_VIEW_POINT[0], DEFAULT_VIEW_POINT[1], DEFAULT_VIEW_POINT[2]);
  glm::vec3 viewCenter(DEFAULT_VIEW_CENTER[0], DEFAULT_VIEW_CENTER[1], DEFAULT_VIEW_CENTER[2]);
  glm::vec3 upVector(DEFAULT_UP_VECTOR[0], DEFAULT_UP_VECTOR[1], DEFAULT_UP_VECTOR[2]);

  float aspect = (w / (float)h);
  m_viewer = new Viewer(viewPoint, viewCenter, upVector, 90.f, aspect);
}
void MyGlWindow::drawMesh(glm::mat4 & view, glm::mat4 & projection)
{
  glm::vec4 lightPos = glm::vec4{global::lightPos, 1.0};
  
  glm::mat4 model{1.0f};
  //model = glm::scale(model, {1,1,1});

  glm::mat4 mview = view * model;
  glm::mat4 mvp = projection * view * model;

  glm::mat4 imvp = glm::inverse(mview);
  glm::mat3 nmat = glm::mat3(glm::transpose(imvp));

  Shader* shader = m_model.shader;
  shader->use();
  shader->setMat4("ModelView", mview);
  shader->setMat4("MVP", mvp);
  shader->setMat3("NormalMatrix", nmat);
  shader->setVec4("LightPosition", view*lightPos);

  glm::vec3 ambient = glm::vec3(0.3, 0.3, 0.3);
  glm::vec3 diffuse = glm::vec3(0.8, 0.8, 0.8);
  glm::vec3 specular = glm::vec3(0.3, 0.3, 0.3);
  float	shiness = 10.0f;

  shader->setVec3("Kd", diffuse);
  shader->setVec3("Ka", ambient);
  shader->setVec3("Ks", specular);
  shader->setFloat("shininess", shiness);

  shader->setVec3("LightIntensity", global::lightIntensity);

  shader->setVec4("Spot.position", global::spotlight.pos);
  shader->setVec3("Spot.intensity", global::spotlight.intensity);
  shader->setVec3("Spot.direction", global::spotlight.direction);
  shader->setFloat("Spot.cutoff", global::spotlight.cutOff);
  shader->setFloat("Spot.innerCutoff", global::spotlight.innerOff);
  shader->setFloat("Spot.exponent", global::spotlight.exponent);

  m_model.Draw();
}

void MyGlWindow::draw(void)
{
  glViewport(0, 0, m_width, m_height);
  glClearColor(global::backGround[0], global::backGround[1], global::backGround[2], 1.f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


  glEnable(GL_DEPTH_TEST);

  const float ratio = global::win->getWindowRatio();
  const float fov = global::win->m_viewer->getAspectRatio();
  const float znear = global::win->getNear();
  const float zfar = global::win->getFar();

  const glm::vec3 eye = m_viewer->getViewPoint(); // m_viewer->getViewPoint().x(), m_viewer->getViewPoint().y(), m_viewer->getViewPoint().z());
  const glm::vec3 look = m_viewer->getViewCenter();   //(m_viewer->getViewCenter().x(), m_viewer->getViewCenter().y(), m_viewer->getViewCenter().z());
  const glm::vec3 up = m_viewer->getUpVector(); // m_viewer->getUpVector().x(), m_viewer->getUpVector().y(), m_viewer->getUpVector().z());

  glm::mat4 view = lookAt(eye, look, up);
  static auto vecToString = [](glm::vec3 a)
  {
    return std::string{std::to_string(a.x) + ", " + std::to_string(a.y) + ", " + std::to_string(a.z)};
  };
  //std::cout << "Camere : " << vecToString(eye) << " Looking : " << vecToString(look) << " Up : " << vecToString(up) << '\n';
  glm::mat4 projection = glm::perspective(ratio, fov, znear, zfar);

  Model::shader->setVec3("camPos", eye);
  drawMesh(view, projection);
  if(updateSnow)
    m_snow.Draw();
  if(updateRain)
    m_rain.Draw();
}

void MyGlWindow::update()
{ 
  TimeManager::instance().Update();

  /////////// ImGUI
  ImGui_ImplOpenGL3_NewFrame();
  ImGui_ImplGlfw_NewFrame();
  ImGui::NewFrame();
  glm::vec3 eye = m_viewer->getViewPoint();
  glm::vec3 look = m_viewer->getViewCenter();
  glm::vec3 up = m_viewer->getUpVector();
  float ratio = global::win->getWindowRatio();
  float fov = global::win->m_viewer->getAspectRatio();
  float znear = global::win->getNear();
  float zfar = global::win->getFar();

  if(ImGui::Begin("DEBUGING"))
  {
    ImGui::InputFloat3("Camera Position", &eye[0]);
    ImGui::InputFloat3("Camera look", &look[0]);
    ImGui::InputFloat3("Camera Up", &up[0]);
    ImGui::InputFloat("Camera Ratio",&ratio);
    ImGui::InputFloat("Camera fov", &fov);
    ImGui::InputFloat("Camera Near", &znear);
    ImGui::InputFloat("Camera Far", &zfar);

  }ImGui::End();
  if(ImGui::Begin(u8"Light Settings"))
  {
    double dt = TimeManager::instance().dt();
    ImGui::SetWindowFontScale(1);
    ImGui::InputDouble("Delta Time", &dt, 0.0, 0.0, "%0.12f");

    ImGui::ColorEdit3("BackGround Color", &global::backGround[0]);
    ImGui::SliderFloat3("light Position", &global::lightPos[0],-10.f,10.f);
    ImGui::ColorEdit3("light Intensity", &global::lightIntensity[0]);

    ImGui::Text("SpotLight");
    ImGui::BeginChild("SpotLight");
    {
      float AngleDegree = glm::degrees<float>(global::spotlight.cutOff);
      float innerAngleDegree = glm::degrees<float>(global::spotlight.innerOff);
      ImGui::SliderFloat4("light Position", &global::spotlight.pos[0], -10.f, 10.f);
      ImGui::SliderFloat3("light Direction", &global::spotlight.direction[0], -1.f, 1.f);
      ImGui::ColorEdit3("light Intensity", &global::spotlight.intensity[0]);
      ImGui::SliderFloat("light Cutoff Angle", &AngleDegree, 0.f,180.f);
      ImGui::SliderFloat("light Inner Cutoff Angle", &innerAngleDegree, 0.f, AngleDegree);
      ImGui::SliderFloat("light exponent", &global::spotlight.exponent, 1.f, 10000.f);

      global::spotlight.cutOff = glm::radians<float>(AngleDegree);
      global::spotlight.innerOff = glm::radians<float>(innerAngleDegree);

    }ImGui::EndChild();

  }ImGui::End();
  if(ImGui::Begin(u8"Snow Settings"))
  {
    auto velo = m_snow.GetGlobalVelocity();

    static bool isUpdate = m_snow.IsPhysicsUpdate();
    static bool isReGen = m_snow.IsRegeneration();

    ImGui::SetWindowFontScale(1);

    ImGui::Checkbox("Draw and update Snow", &updateSnow);
    ImGui::SliderFloat3("All - Snow Speed", &velo[0], -50.0f, 15.0f);
    m_snow.SetGlobalVelocity(velo);

    ImGui::Checkbox("Physics updating", &isUpdate);
    ImGui::Checkbox("Regeneration updating", &isReGen);
    m_snow.SetPhysicsUpdate(isUpdate);
    m_snow.SetRegeneration(isReGen);

  }ImGui::End();

  if(ImGui::Begin(u8"Rain Settings"))
  {
    auto velo = m_rain.GetGlobalVelocity();

    static bool isUpdate = m_rain.IsPhysicsUpdate();
    static bool isReGen = m_rain.IsRegeneration();

    ImGui::SetWindowFontScale(1);

    ImGui::Checkbox("Draw and update Rain", &updateRain);
    ImGui::SliderFloat3("All - Rain Speed", &velo[0], -50.0f, 15.0f);
    ImGui::SliderFloat2("Rain particle's size", &m_rain.rainSize[0],0.0f,0.1f,"%.6f");
    m_rain.SetGlobalVelocity(velo);

    ImGui::Checkbox("Physics updating", &isUpdate);
    ImGui::Checkbox("Regeneration updating", &isReGen);
    m_rain.SetPhysicsUpdate(isUpdate);
    m_rain.SetRegeneration(isReGen);
  }ImGui::End();

  if(updateSnow)
    m_snow.Update();
  if(updateRain)
    m_rain.Update();
}

void MyGlWindow::init()
{ 
  initialize();

  m_snow.Init();
  m_rain.Init();

  TimeManager::instance().Init();
}

MyGlWindow::~MyGlWindow()
{
  //Bind 0, which means render to back buffer, as a result, fb is unbound
  glBindFramebuffer(GL_FRAMEBUFFER_EXT, 0);

  //delete m_mesh;
}
constexpr char ObjectName[] = {"Models/untitled.obj"};

void MyGlWindow::initialize()
{
  m_model.loadModel(ObjectName);
  Model::shader = new Shader{"shaders/model.vert","shaders/model.frag"};
  Shader* sd = Model::shader;
  sd->setInt("hasTextureDiffuse", false);
  sd->setInt("hasTextureSpecular", false);
}



