
#ifndef  __GLOBAL
#define  __GLOBAL

#include <glm/glm.hpp>
#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/vec2.hpp>

class MyGlWindow; 
struct SpotLight
{
  glm::vec4 pos = glm::vec4(0.0f, 0.0f, 10.0f, 1.0f);
  glm::vec3 intensity = glm::vec3(1.0, 0.0, 0.0);
  glm::vec3 direction = glm::vec3(0.0, 0.0f, 0.0);
  float cutOff = glm::radians<float>(60.f);
  float innerOff = glm::radians<float>(30.f);
  float exponent = 100.f;
};

namespace global
{
	extern glm::vec3 lightPos;
	extern glm::vec3 backGround;
  extern glm::vec3 lightIntensity;

  extern MyGlWindow* win;
  extern SpotLight spotlight;

};
#endif
