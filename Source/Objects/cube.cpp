
#include "cube.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>

colorCube::colorCube() 
  : shader(VertFileName, FragFileName)
{

	setup();
}



void colorCube::quad(std::vector<glm::vec4>  & cube_vertices, std::vector<glm::vec3> & cube_normals, glm::vec4 positions[], int a, int b, int c, int d)
{

	cube_vertices.push_back(positions[a]);
	cube_vertices.push_back(positions[b]);
	cube_vertices.push_back(positions[c]);

	glm::vec3 vec_a = glm::vec3(positions[b] - positions[a]);
	glm::vec3 vec_b = glm::vec3(positions[c] - positions[a]);
	glm::vec3 n1 = glm::cross(vec_a, vec_b);
	glm::normalize(n1);


	cube_normals.push_back(n1);
	cube_normals.push_back(n1);
	cube_normals.push_back(n1);



	cube_vertices.push_back(positions[a]);
	cube_vertices.push_back(positions[c]);
	cube_vertices.push_back(positions[d]);

	glm::vec3 vec_c = glm::vec3(positions[c] - positions[a]);
	glm::vec3 vec_d = glm::vec3(positions[d] - positions[a]);
	glm::vec3 n2 = glm::cross(vec_c, vec_d);
	glm::normalize(n2);


	cube_normals.push_back(n2);
	cube_normals.push_back(n2);
	cube_normals.push_back(n2);



}


/*

void initVAO(void)
{
GLfloat vertices[] =
{
-1.0f, -1.0f,
1.0f, -1.0f,
1.0f, 1.0f,
-1.0f, 1.0f,
};

GLfloat texcoords[] =
{
1.0f, 1.0f,
0.0f, 1.0f,
0.0f, 0.0f,
1.0f, 0.0f
};

GLushort indices[] = { 0, 1, 3, 3, 1, 2 };

GLuint vao;
glGenVertexArrays(1, &vao);
glBindVertexArray(vao);

GLuint vertexBufferObjID[3];
glGenBuffers(3, vertexBufferObjID);

glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjID[0]);
glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
glVertexAttribPointer((GLuint)positionLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
glEnableVertexAttribArray(positionLocation);

glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjID[1]);
glBufferData(GL_ARRAY_BUFFER, sizeof(texcoords), texcoords, GL_STATIC_DRAW);
glVertexAttribPointer((GLuint)texcoordsLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
glEnableVertexAttribArray(texcoordsLocation);

glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexBufferObjID[2]);
glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
}



*/
std::vector<glm::vec3> cube_normals;
std::vector<glm::vec4> cube_vertices;
void colorCube::setup()
{

	
	/*	std::vector<GLushort> cube_elements;*/

	glm::vec4 positions[8] = {
		glm::vec4(-0.5f, -0.5f, 0.5f, 1.0f),  //0
		glm::vec4(-0.5f, 0.5f, 0.5f, 1.0f), //1
		glm::vec4(0.5f, 0.5f, 0.5f, 1.0f), //2
		glm::vec4(0.5f, -0.5f, 0.5f, 1.0f), //3
		glm::vec4(-0.5f, -0.5f, -0.5f, 1.0f), //4
		glm::vec4(-0.5f, 0.5f, -0.5f, 1.0f), //5
		glm::vec4(0.5f, 0.5f, -0.5f, 1.0f), //6
		glm::vec4(0.5f, -0.5f, -0.5f, 1.0f) //7
	};


	float tex[36 * 2] = {
		// Front
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		// Right
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		// Back
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		// Left
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		// Bottom
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		// Top
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f
	};


	quad(cube_vertices, cube_normals, positions, 0, 1, 2, 3);
	quad(cube_vertices, cube_normals, positions, 2, 3, 7, 6);
	quad(cube_vertices, cube_normals, positions, 3, 0, 4, 7);
	quad(cube_vertices, cube_normals, positions, 6, 5, 1, 2);
	quad(cube_vertices, cube_normals, positions, 4, 5, 6, 7);
	quad(cube_vertices, cube_normals, positions, 5, 4, 0, 1);


	//  	for (int i = 0; i < 36; i++)
	//  		cube_elements.push_back(i);

	//load shaders
	shaderProgram->initFromFiles("texture.vert", "texture.frag");

	//create vao
	glGenVertexArrays(1, &vaoHandle);
	glBindVertexArray(vaoHandle);

	//add attributes and uniform vars
	shaderProgram->addAttribute("VertexPosition");
	shaderProgram->addAttribute("VertexNormal");
	shaderProgram->addAttribute("VertexTexCoord");


	shaderProgram->addUniform("Light.Position");
	shaderProgram->addUniform("Light.Intensity");
	

	shaderProgram->addUniform("Material.Ka");
	shaderProgram->addUniform("Material.Kd");
	shaderProgram->addUniform("Material.Ks");
	shaderProgram->addUniform("Material.Shiness");

	shaderProgram->addUniform("ModelViewMatrix");
	shaderProgram->addUniform("NormalMatrix");
	shaderProgram->addUniform("MVP");
	shaderProgram->addUniform("Tex1");
	//	shaderProgram->addUniform("Inverse_modelview");



	//create vbo for vertices
	glGenBuffers(1, &vbo_cube_vertices);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_vertices);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*cube_vertices.size() * 4, cube_vertices.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(
		shaderProgram->attribute("VertexPosition"), // attribute
		4,                 // number of elements per vertex, here (x,y,z,1)
		GL_FLOAT,          // the type of each element
		GL_FALSE,          // take our values as-is
		0,                 // no extra data between each position
		0                  // offset of first element
		);
	glEnableVertexAttribArray(shaderProgram->attribute("VertexPosition"));


	//create vbo for colors
	glGenBuffers(1, &vbo_cube_normals);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*cube_normals.size() * 3, cube_normals.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(
		shaderProgram->attribute("VertexNormal"), // attribute
		3,                 // number of elements per vertex, here (R,G,B)
		GL_FLOAT,          // the type of each element
		GL_FALSE,          // take our values as-is
		0,                 // no extra data between each position
		0                  // offset of first element
		);
	glEnableVertexAttribArray(shaderProgram->attribute("VertexNormal"));


	//create vbo for colors
	glGenBuffers(1, &vbo_cube_texture);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_texture);
	glBufferData(GL_ARRAY_BUFFER, 36 * 2 * sizeof(float), tex, GL_STATIC_DRAW);
	glVertexAttribPointer(
		shaderProgram->attribute("VertexTexCoord"), // attribute
		2,                 // number of elements per vertex, here (R,G,B)
		GL_FLOAT,          // the type of each element
		GL_FALSE,          // take our values as-is
		0,                 // no extra data between each position
		0                  // offset of first element
		);
	glEnableVertexAttribArray(shaderProgram->attribute("VertexTexCoord"));


	// 	glGenBuffers(1, &ibo_cube_elements);
	// 	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_cube_elements);
	// 	glBufferData(GL_ELEMENT_ARRAY_BUFFER, cube_elements.size() * sizeof(cube_elements[0]), cube_elements.data(), GL_STATIC_DRAW);


	glBindVertexArray(0);


}

void colorCube::draw(glm::mat4 model, glm::mat4 view, glm::mat4 projection)
{

	


	glm::mat4 mview = view * model;
	glm::mat4 mvp = projection * view * model;

	glm::vec4 lightPos(10, 10, 10, 1);
	glm::vec3 LightIntensity(0.8, 0.8, 0.8);
	glm::vec3 Ka(0.3, 0.3, 0.3);
	glm::vec3 Kd(1, 1, 1);
	glm::vec3 Ks(0.3, 0.3, 0.3);

	GLfloat shiness = 10;
	
	
	glm::mat4 imvp = glm::inverse(mview);
	glm::mat3 nmat = glm::mat3(glm::transpose(imvp));


	shaderProgram->use();

	glUniform4fv(shaderProgram->uniform("Light.Position"), 1, glm::value_ptr(mview*lightPos));
	glUniform3fv(shaderProgram->uniform("Light.Intensity"), 1, glm::value_ptr(LightIntensity));
	

	glUniform3fv(shaderProgram->uniform("Material.Ka"), 1, glm::value_ptr(Ka));
	glUniform3fv(shaderProgram->uniform("Material.Kd"), 1, glm::value_ptr(Kd));
	glUniform3fv(shaderProgram->uniform("Material.Ks"), 1, glm::value_ptr(Ks));
	glUniform1fv(shaderProgram->uniform("Material.Shiness"), 1, &shiness);

	glUniformMatrix4fv(shaderProgram->uniform("ModelViewMatrix"), 1, GL_FALSE, glm::value_ptr(mview));
	glUniformMatrix3fv(shaderProgram->uniform("NormalMatrix"), 1, GL_FALSE, glm::value_ptr(nmat));
	glUniformMatrix4fv(shaderProgram->uniform("MVP"), 1, GL_FALSE, glm::value_ptr(mvp));

	
	glUniform1i(shaderProgram->uniform("Tex1"), 0);
	

	glBindTexture(GL_TEXTURE_2D, tex_2d);
	glBindVertexArray(vaoHandle);
	glDrawArrays(GL_TRIANGLES, 0, cube_vertices.size() * 3);
	glBindTexture(GL_TEXTURE_2D, 0);
	shaderProgram->disable();
	
}







////////////////////////////////////////////////
float floorColor1[3] = { .7f, .7f, .7f }; // Light color
float floorColor2[3] = { .3f, .3f, .3f }; // Dark color

checkeredFloor::checkeredFloor()
{

	setup(50, 16);
}

int nvert;

void checkeredFloor::setup(float size, int nSquares)
{
	std::vector <glm::vec4> vlists;
	std::vector <glm::vec3> clists;


	// parameters:
	float maxX = size / 2, maxY = size / 2;
	float minX = -size / 2, minY = -size / 2;

	int x, y, v[3], i;
	float xp, yp, xd, yd;
	v[2] = 0;
	xd = (maxX - minX) / ((float)nSquares);
	yd = (maxY - minY) / ((float)nSquares);


	for (x = 0, xp = minX; x < nSquares; x++, xp += xd) {
		for (y = 0, yp = minY, i = x; y < nSquares; y++, i++, yp += yd) {
			if (i % 2 == 1) {
				clists.push_back(glm::vec3(floorColor1[0], floorColor1[1], floorColor1[2]));
				clists.push_back(glm::vec3(floorColor1[0], floorColor1[1], floorColor1[2]));
				clists.push_back(glm::vec3(floorColor1[0], floorColor1[1], floorColor1[2]));
				clists.push_back(glm::vec3(floorColor1[0], floorColor1[1], floorColor1[2]));
				clists.push_back(glm::vec3(floorColor1[0], floorColor1[1], floorColor1[2]));
				clists.push_back(glm::vec3(floorColor1[0], floorColor1[1], floorColor1[2]));
			}
			else {
				clists.push_back(glm::vec3(floorColor2[0], floorColor2[1], floorColor2[2]));
				clists.push_back(glm::vec3(floorColor2[0], floorColor2[1], floorColor2[2]));
				clists.push_back(glm::vec3(floorColor2[0], floorColor2[1], floorColor2[2]));
				clists.push_back(glm::vec3(floorColor2[0], floorColor2[1], floorColor2[2]));
				clists.push_back(glm::vec3(floorColor2[0], floorColor2[1], floorColor2[2]));
				clists.push_back(glm::vec3(floorColor2[0], floorColor2[1], floorColor2[2]));

			}
			vlists.push_back(glm::vec4(xp, 0, yp, 1));
			vlists.push_back(glm::vec4(xp, 0, yp + yd, 1));
			vlists.push_back(glm::vec4(xp + xd, 0, yp + yd, 1));

			vlists.push_back(glm::vec4(xp, 0, yp, 1));
			vlists.push_back(glm::vec4(xp + xd, 0, yp + yd, 1));
			vlists.push_back(glm::vec4(xp + xd, 0, yp, 1));



		} // end of for j
	}// end of for i

	nvert = vlists.size();

	shaderProgram = new ShaderProgram();

	//load shaders
	shaderProgram->initFromFiles("floor.vert", "floor.frag");

	//create vao
	glGenVertexArrays(1, &vaoHandle);
	glBindVertexArray(vaoHandle);

	//add attributes and uniform vars
	shaderProgram->addAttribute("VertexPosition");
	shaderProgram->addAttribute("VertexColor");

	shaderProgram->addUniform("MVP");


	//create vbo for vertices
	glGenBuffers(1, &vbo_cube_vertices);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_vertices);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vlists.size() * 4, vlists.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(
		shaderProgram->attribute("VertexPosition"), // attribute
		4,                 // number of elements per vertex, here (x,y,z,1)
		GL_FLOAT,          // the type of each element
		GL_FALSE,          // take our values as-is
		0,                 // no extra data between each position
		0                  // offset of first element
		);
	glEnableVertexAttribArray(shaderProgram->attribute("VertexPosition"));


	//create vbo for colors
	glGenBuffers(1, &vbo_cube_colors);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_colors);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*clists.size() * 3, clists.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(
		shaderProgram->attribute("VertexColor"), // attribute
		3,                 // number of elements per vertex, here (R,G,B)
		GL_FLOAT,          // the type of each element
		GL_FALSE,          // take our values as-is
		0,                 // no extra data between each position
		0                  // offset of first element
		);
	glEnableVertexAttribArray(shaderProgram->attribute("VertexColor"));

	vlists.clear();
	clists.clear();

	glBindVertexArray(0);
}


void checkeredFloor::draw(glm::mat4 model, glm::mat4 view, glm::mat4 projection)
{

	glm::mat4 mview = view * model;
	glm::mat4 mvp = projection * view * model;


	// 
	// 	glm::vec4 lightPos(100, 100, 100, 1);
	// 	glm::vec3 Ka(0.1, 0.1, 0.1);
	// 	glm::vec3 Kd(1, 1, 0);
	// 	glm::vec3 Ks(1, 1, 0);
	// 
	// 	GLfloat shiness = 0.1;
	// 
	// 	glm::vec3 La(0.1, 0.1, 0.1);
	// 	glm::vec3 Ld(0.5, 0.5, 0.5);
	// 	glm::vec3 Ls(0.2, 0.2, 0.2);
	// 	glm::mat4 imvp = glm::inverse(mview);

	// 	glm::mat3 nmat = glm::mat3(glm::transpose(imvp));



	shaderProgram->use();

	glUniformMatrix4fv(shaderProgram->uniform("MVP"), 1, GL_FALSE, glm::value_ptr(mvp));


	glBindVertexArray(vaoHandle);
	glDrawArrays(GL_TRIANGLES, 0, nvert * 3);


	shaderProgram->disable();

}



GLuint nVerts, elements;
float radius;
GLuint slices, stacks;

sphere::sphere(float rad, GLuint sl, GLuint st)
{

	radius = rad;
	slices = sl;
	stacks = st;
	setup(rad, sl, st);

	
}


void sphere::generateVerts(float * verts, float * norms, float * tex, unsigned int * el)
{
	// Generate positions and normals
	GLfloat theta, phi;
	GLfloat thetaFac = glm::two_pi<float>() / slices;
	GLfloat phiFac = glm::pi<float>() / stacks;
	GLfloat nx, ny, nz, s, t;
	GLuint idx = 0, tIdx = 0;
	for (GLuint i = 0; i <= slices; i++) {
		theta = i * thetaFac;
		s = (GLfloat)i / slices;
		for (GLuint j = 0; j <= stacks; j++) {
			phi = j * phiFac;
			t = (GLfloat)j / stacks;
			nx = sinf(phi) * cosf(theta);
			ny = sinf(phi) * sinf(theta);
			nz = cosf(phi);
			verts[idx] = radius * nx; verts[idx + 1] = radius * ny; verts[idx + 2] = radius * nz;
			norms[idx] = nx; norms[idx + 1] = ny; norms[idx + 2] = nz;
			idx += 3;

			tex[tIdx] = s;
			tex[tIdx + 1] = t;
			tIdx += 2;
		}
	}

	// Generate the element list
	idx = 0;
	for (GLuint i = 0; i < slices; i++) {
		GLuint stackStart = i * (stacks + 1);
		GLuint nextStackStart = (i + 1) * (stacks + 1);
		for (GLuint j = 0; j < stacks; j++) {
			if (j == 0) {
				el[idx] = stackStart;
				el[idx + 1] = stackStart + 1;
				el[idx + 2] = nextStackStart + 1;
				idx += 3;
			}
			else if (j == stacks - 1) {
				el[idx] = stackStart + j;
				el[idx + 1] = stackStart + j + 1;
				el[idx + 2] = nextStackStart + j;
				idx += 3;
			}
			else {
				el[idx] = stackStart + j;
				el[idx + 1] = stackStart + j + 1;
				el[idx + 2] = nextStackStart + j + 1;
				el[idx + 3] = nextStackStart + j;
				el[idx + 4] = stackStart + j;
				el[idx + 5] = nextStackStart + j + 1;
				idx += 6;
			}
		}
	}
}


void sphere::setup(float rad, GLuint sl, GLuint st)
{

	nVerts = (slices + 1) * (stacks + 1);
	elements = (slices * 2 * (stacks - 1)) * 3;

	// Verts
	float * v = new float[3 * nVerts];
	// Normals
	float * n = new float[3 * nVerts];
	// Tex coords
	float * tex = new float[2 * nVerts];
	// Elements
	unsigned int * el = new unsigned int[elements];

	// Generate the vertex data
	generateVerts(v, n, tex, el);

	shaderProgram = new ShaderProgram();

	//load shaders
	shaderProgram->initFromFiles("blinnphong.vert", "blinnphong.frag");

	//create vao
	glGenVertexArrays(1, &vaoHandle);
	glBindVertexArray(vaoHandle);



	shaderProgram->addAttribute("VertexPosition");
	shaderProgram->addAttribute("VertexNormal");
	//shaderProgram->addAttribute("VertexTexCoord");


	shaderProgram->addUniform("LightPosition");
	shaderProgram->addUniform("LightIntensity");


	shaderProgram->addUniform("Ka");
	shaderProgram->addUniform("Kd");
	shaderProgram->addUniform("Ks");
	shaderProgram->addUniform("shiness");

	shaderProgram->addUniform("ModelViewMatrix");
	shaderProgram->addUniform("NormalMatrix");
	shaderProgram->addUniform("MVP");


	//create vbo for vertices
	glGenBuffers(1, &vbo_cube_vertices);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_vertices);
	glBufferData(GL_ARRAY_BUFFER, (3 * nVerts) * sizeof(float), v, GL_STATIC_DRAW);
	glVertexAttribPointer(
		shaderProgram->attribute("VertexPosition"), // attribute
		3,                 // number of elements per vertex, here (x,y,z,1)
		GL_FLOAT,          // the type of each element
		GL_FALSE,          // take our values as-is
		0,                 // no extra data between each position
		0                  // offset of first element
		);
	glEnableVertexAttribArray(shaderProgram->attribute("VertexPosition"));


	//create vbo for colors
	glGenBuffers(1, &vbo_cube_normals);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_normals);
	glBufferData(GL_ARRAY_BUFFER, (3 * nVerts) * sizeof(float), n, GL_STATIC_DRAW);
	glVertexAttribPointer(
		shaderProgram->attribute("VertexNormal"), // attribute
		3,                 // number of elements per vertex, here (R,G,B)
		GL_FLOAT,          // the type of each element
		GL_FALSE,          // take our values as-is
		0,                 // no extra data between each position
		0                  // offset of first element
		);
	glEnableVertexAttribArray(shaderProgram->attribute("VertexNormal"));


	//create vbo for colors
// 	glGenBuffers(1, &vbo_cube_texture);
// 	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_texture);
// 	glBufferData(GL_ARRAY_BUFFER, (2 * nVerts) * sizeof(float), tex, GL_STATIC_DRAW);
// 	glVertexAttribPointer(
// 		shaderProgram->attribute("VertexTexCoord"), // attribute
// 		2,                 // number of elements per vertex, here (R,G,B)
// 		GL_FLOAT,          // the type of each element
// 		GL_FALSE,          // take our values as-is
// 		0,                 // no extra data between each position
// 		0                  // offset of first element
// 		);
//	glEnableVertexAttribArray(shaderProgram->attribute("VertexTexCoord")

	glGenBuffers(1, &ibo_cube_elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_cube_elements);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements * sizeof(unsigned int), el, GL_STATIC_DRAW);



	delete[] v;
	delete[] n;
	delete[] el;
	delete[] tex;


	glBindVertexArray(0);
}



void sphere::draw(glm::mat4 & model, glm::mat4 & view, glm::mat4 & projection)
{

	glm::mat4 mview = view * model;
	glm::mat4 mvp = projection * view * model;

	glm::vec4 lightPos(10, 10, 10, 1);
	glm::vec3 LightIntensity(0.8, 0.8, 0.8);
	glm::vec3 Ka(0.3, 0.3, 0.3);
	glm::vec3 Kd(0.7, 0.7, 0.7);
	glm::vec3 Ks(0.3, 0.3, 0.3);

	GLfloat shiness = 3;


	glm::mat4 imvp = glm::inverse(mview);
	glm::mat3 nmat = glm::mat3(glm::transpose(imvp));


	shaderProgram->use();

	glUniform4fv(shaderProgram->uniform("LightPosition"), 1, glm::value_ptr(lightPos));
	glUniform3fv(shaderProgram->uniform("LightIntensity"), 1, glm::value_ptr(LightIntensity));


	glUniform3fv(shaderProgram->uniform("Ka"), 1, glm::value_ptr(Ka));
	glUniform3fv(shaderProgram->uniform("Kd"), 1, glm::value_ptr(Kd));
	glUniform3fv(shaderProgram->uniform("Ks"), 1, glm::value_ptr(Ks));
	glUniform1fv(shaderProgram->uniform("shiness"), 1, &shiness);

	glUniformMatrix4fv(shaderProgram->uniform("ModelViewMatrix"), 1, GL_FALSE, glm::value_ptr(mview));
	glUniformMatrix3fv(shaderProgram->uniform("NormalMatrix"), 1, GL_FALSE, glm::value_ptr(nmat));
	glUniformMatrix4fv(shaderProgram->uniform("MVP"), 1, GL_FALSE, glm::value_ptr(mvp));


	glBindVertexArray(vaoHandle);

	glDrawElements(GL_TRIANGLES, elements, GL_UNSIGNED_INT, 0);


	shaderProgram->disable();

}

