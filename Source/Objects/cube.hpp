#ifndef __CUBE
#define __CUBE

#include <GL/glew.h>
#include <glm/mat4x4.hpp>

//#include "Engine/Loader.hpp"
#include "Engine/Shader.hpp"
#include <vector>


class colorCube
{
public:
	colorCube();
	//virtual ~colorCube();
	void setup();
	void draw(glm::mat4 model, glm::mat4 view, glm::mat4 projection);
	void quad(std::vector<glm::vec4>  &cube_vertices, std::vector<glm::vec3> & cube_normals, glm::vec4 positions[], int a, int b, int c, int d);

	GLuint vaoHandle;
	GLuint tex_2d;
	GLuint vbo_cube_vertices, vbo_cube_normals, vbo_cube_texture, ibo_cube_elements;
	//ShaderProgram *shaderProgram;
  static Shader *shader;
private:
  static constexpr char* VertFileName = {"shaders/texture.vert"};
  static constexpr char* FragFileName = {"shaders/texture.frag"};
};



class checkeredFloor
{
public:
	checkeredFloor();
	void setup(float size, int nSquares);
	void draw(glm::mat4 model, glm::mat4 view, glm::mat4 projection);

	GLuint vaoHandle;

	GLuint vbo_cube_vertices, vbo_cube_colors;
	Shader shader;
private:
  static constexpr char* VertFileName = {""};
  static constexpr char* FragFileName = {""};
};

class sphere
{

public:
	sphere(float rad, GLuint sl, GLuint st);
	void setup(float rad, GLuint sl, GLuint st);
	void draw(glm::mat4 & model, glm::mat4 & view, glm::mat4  & projection);
	void generateVerts(float * verts, float * norms, float * tex, unsigned int * el);

	GLuint vaoHandle;

	GLuint vbo_cube_vertices, vbo_cube_texture, vbo_cube_normals;
	GLuint ibo_cube_elements;
	Shader shader;

private:
  static constexpr char* VertFileName = {""};
  static constexpr char* FragFileName = {""};
};


#endif