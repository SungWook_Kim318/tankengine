
//#define GLFW_INCLUDE_GLU

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "global.hpp"
#include "MyGlWindow.hpp"
#include "Engine/CallbackFunctions.hpp"
#include "Engine/TimeManager.hpp"

//IMGUI
#include "Externs/IMGUI/imgui.h"
#include "Externs/IMGUI/imgui_impl_glfw.h"
#include "Externs/IMGUI/imgui_impl_opengl3.h"

int main(void)
{
  GLFWwindow* window = nullptr;
  TimeManager& TM = TimeManager::instance();

  /* Initialize the library */
  if(!glfwInit())
  {
    return -1;
  }


  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


  int width = 1280;
  int height = 720;

  /* Create a windowed mode window and its OpenGL context */
  window = glfwCreateWindow(width, height, "OpenGL FrameWork", NULL, NULL);
  if(!window)
  {
    glfwTerminate();
    return -1;
  }

  /* IMGUI INIT */
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO();
  (void)io;

  io.Fonts->AddFontFromFileTTF("fonts/D2Coding.ttf", 14.f, NULL, io.Fonts->GetGlyphRangesKorean());

  const char glsl_version[] = "#version 450";
  ImGui_ImplGlfw_InitForOpenGL(window, true);
  ImGui_ImplOpenGL3_Init(glsl_version);

  ImGui::StyleColorsDark();


  glfwMakeContextCurrent(window);


  /* Make the window's context current */

  glewExperimental = GL_TRUE;
  GLenum err = glewInit();
  if(err != GLEW_OK)
  {
    //Problem: glewInit failed, something is seriously wrong.
    std::cout << "glewInit failed, aborting." << std::endl;
    return 0;
  }


  printf("OpenGL %s, GLSL %s\n", glGetString(GL_VERSION),
         glGetString(GL_SHADING_LANGUAGE_VERSION));


  glfwSwapInterval(0);  //disable vsync




  global::win = new MyGlWindow(width, height);

  bool show_test_window = true;
  bool show_another_window = false;


  double previousTime = glfwGetTime();
  int frameCount = 0;




  glfwSetWindowSizeCallback(window, window_size_callback);
  glfwSetMouseButtonCallback(window, mouse_button_callback);
  glfwSetCursorPosCallback(window, cursor_pos_callback);
  glfwSetKeyCallback(window, key_callback);


  glfwSetWindowTitle(window, "myGlWindow");

  global::win->init();

  /* Loop until the user closes the window */
  while(!glfwWindowShouldClose(window))
  {
    /* Poll for and process events */
    glfwPollEvents();

    //TM.Update();
    global::win->update();
    glClearColor(0.2f, 0.2f, .2f, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_DEPTH_TEST);

    int display_w, display_h;
    glfwGetFramebufferSize(window, &display_w, &display_h);
    glViewport(0, 0, display_w, display_h);

    global::win->draw();

    mouseDragging(display_w, display_h);
    ///////// Render ImGUI
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    /* Swap front and back buffers */
    glfwSwapBuffers(window);
  }

  glfwDestroyWindow(window);

  glfwTerminate();
  return 0;
}