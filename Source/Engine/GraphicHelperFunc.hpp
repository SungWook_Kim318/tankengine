#pragma once

namespace GraphicHelper
{
  unsigned loadTexture(const char *path, bool gammaCorrection);
}