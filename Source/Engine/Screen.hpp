#ifndef __SCREEN
#define __SCREEN

#include <GL/glew.h>
#include <glm/mat4x4.hpp>

#include <vector>

class Shader;

class Screen
{
public:
	Screen();
	~Screen();

	void setup();
	void draw();

	GLuint vaoHandle;
	GLuint vbo_vertices;
	Shader* shader;


};


#endif
