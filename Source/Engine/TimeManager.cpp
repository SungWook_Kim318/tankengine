#include "TimeManager.hpp"

#include <iostream>

TimeManager::~TimeManager()
{

}

TimeManager& TimeManager::instance()
{
  static TimeManager* inst = new TimeManager{};
  return *inst;
}

void TimeManager::Init()
{
  m_startTime = std::chrono::high_resolution_clock::now();
  m_min = 1000000000u;
}

void TimeManager::Update()
{
  m_currentTime = std::chrono::high_resolution_clock::now();
  m_nextDeltaTime = std::chrono::duration_cast<std::chrono::duration<double>>(m_currentTime - m_startTime);
  m_startTime = m_currentTime;

#ifdef _DEBUG
  unsigned cur = 1 / m_nextDeltaTime.count();
  m_average += cur / 2;
  if(m_max < cur)
    m_max = cur;
  if(m_min > cur)
    m_min = cur;
#endif
}

float TimeManager::GetFPS(void) const
{
  return 1 / m_nextDeltaTime.count();
}

double TimeManager::dt(void) const
{
  return m_nextDeltaTime.count();
}

double TimeManager::Getdt(void) const
{
  return m_nextDeltaTime.count();
}

void TimeManager::printLog(void) const
{ 
  std::cout << " ";
}
