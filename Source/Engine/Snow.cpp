#include "Snow.hpp"

#include "MyGlWindow.hpp"
#include "global.hpp"

#include "TimeManager.hpp"

//#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/random.hpp>

#include <array>

const static glm::vec4 min{-50.f,-50.f,-50.f, 0.0f};
const static glm::vec4 max{ 50.f, 50.f, 50.f, 0.0f};

Snow::Snow(unsigned numberOfParticle)
  : numParticle{numberOfParticle}, global_Velocity{0.0f,-1.0f,0.0f},
  particle{nullptr}, integration{nullptr},
  positionData{numberOfParticle}, velocityData{numberOfParticle},
  DrawPositionData{numberOfParticle}
{ 
  
}


Snow::~Snow()
{ }


void Snow::Init()
{ 
  //static_assert(cutoffIn < cutoffOut, "RECOMEND SUICIDED.");

  particle = new Shader{"shaders/snow/particle.vert", "shaders/snow/particle.frag", "shaders/snow/particle.geom"};
  integration = new Shader{"shaders/snow/integrate.comp"};
  regeneration = new Shader{"shaders/snow/regeneration.comp"};

  std::cout << "Number of particle : " << numParticle << '\n';
  // debuging----------------------------------------------------------------------
#ifdef _DEBUG
  auto vec4_to_string = [](const glm::vec4& v) -> std::string
  { 
    return std::string{std::to_string(v.x) + ", " + std::to_string(v.y) + ", " + std::to_string(v.z) + ", " + std::to_string(v.w) + '\n'};
  };
  auto vec3_to_string = [](const glm::vec3& v) -> std::string
  {
    return std::string{std::to_string(v.x) + ", " + std::to_string(v.y) + ", " + std::to_string(v.z) + '\n'};
  };
#endif // DEBUG
  for(auto i = 0 ; i < numParticle; ++i)
  {    
    positionData[i] = glm::vec4(glm::linearRand(min, max));
    positionData[i].w = 1.0f;
    velocityData[i] = glm::linearRand(glm::vec4{-1.0f, -0.2f, -1.0f, 0.f}, glm::vec4{1.0f, -0.1f, 1.0f, glm::two_pi<float>()});
  }

  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  glGenBuffers(1, &positions_vbo);
  glGenBuffers(1, &velocities_vbo);
  //glGenBuffers(1, &drawPos_vbo);

  // fill with initial data
  glBindBuffer(GL_ARRAY_BUFFER, positions_vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4)*numParticle, &positionData[0], GL_STATIC_DRAW);

  // fill with initial data
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, velocities_vbo);
  glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(glm::vec4)*numParticle, &velocityData[0], GL_STATIC_DRAW);

  // fill with initial data
  //glBindBuffer(GL_ARRAY_BUFFER, drawPos_vbo);
  //glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4)*numParticle, DrawPositionData.data(), GL_STATIC_DRAW);

  // set up generic attrib pointers
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (char*)0 + 0 * sizeof(GLfloat));

  const GLuint ssbos[] = {positions_vbo, velocities_vbo};
  glBindBuffersBase(GL_SHADER_STORAGE_BUFFER, 0, 2, ssbos);
}

void Snow::Update()
{
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, positions_vbo);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, velocities_vbo);

  auto& TM = TimeManager::instance();
  integration->use();
  integration->setFloat("dt", static_cast<float>(TM.dt()));
  integration->setVec3("global_Velocity", global_Velocity);
  if(isUpdate)
    glDispatchCompute(numParticle / 256, 1, 1);


  regeneration->use();
  regeneration->setVec4("minimum", min);
  regeneration->setVec4("maximum", max);
  if(isRegeneration)
    glDispatchCompute(numParticle/256, 1, 1);
}

void Snow::Draw() const
{
  const float ratio = global::win->getWindowRatio();
  const float fov = global::win->m_viewer->getAspectRatio();
  const float znear = global::win->getNear();
  const float zfar = global::win->getFar();

  glm::vec3 eye = global::win->m_viewer->getViewPoint(); // m_viewer->getViewPoint().x(), m_viewer->getViewPoint().y(), m_viewer->getViewPoint().z());
  glm::vec3 look = global::win->m_viewer->getViewCenter();   //(m_viewer->getViewCenter().x(), m_viewer->getViewCenter().y(), m_viewer->getViewCenter().z());
  glm::vec3 up = global::win->m_viewer->getUpVector(); // m_viewer->getUpVector().x(), m_viewer->getUpVector().y(), m_viewer->getUpVector().z());

  glm::mat4 Projection = glm::perspective(ratio, fov, znear, zfar);
  glm::mat4 View = lookAt(eye, look, up);

  particle->use();

  // set the uniforms
  particle->setMat4("View", View);
  particle->setMat4("Projection", Projection);
  
  // bind the current vao
  glBindVertexArray(vao);
  
  // we are blending so no depth testing
  //glDisable(GL_DEPTH_TEST);
  // enable blending
  glEnable(GL_BLEND);
  //  and set the blend function to result = 1*source + 1*destination
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glDrawArrays(GL_POINTS, 0, numParticle);

  // Set back
  glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);
}

void Snow::Close()
{ }

void Snow::SetGlobalVelocity(glm::vec3& velocity)
{
  global_Velocity = velocity;
}

const glm::vec3 Snow::GetGlobalVelocity(void)
{
  return global_Velocity;
}

void Snow::SetPhysicsUpdate(bool b)
{
  isUpdate = b;
}

void Snow::SetRegeneration(bool b)
{ 
  isRegeneration = b;
}

bool Snow::IsPhysicsUpdate(void)
{
  return isUpdate;
}

bool Snow::IsRegeneration(void)
{
  return isRegeneration;
}
