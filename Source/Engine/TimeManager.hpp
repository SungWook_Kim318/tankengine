#pragma once

#include <chrono>

class TimeManager
{
public:
  static TimeManager& instance();

  //void Create();
  void Init();
  void Update();
  //void Release();
  //void Destroy();

  float GetFPS(void) const;
  template<typename T>
  T GetFPS(void) const
  {
    return 1.0 / m_nextDeltaTime.count();
  }

  double dt(void) const;
  double Getdt(void) const;

  void printLog(void) const;
private:
  constexpr TimeManager()
    :m_currentTime{}, m_nextDeltaTime{}, m_startTime{},
    m_average{}, m_max{}, m_min{}
  { }
  ~TimeManager();

  std::chrono::high_resolution_clock::time_point m_currentTime;
  std::chrono::duration<double> m_nextDeltaTime;
  std::chrono::high_resolution_clock::time_point m_startTime;

  unsigned m_average;
  unsigned m_max;
  unsigned m_min;
};

