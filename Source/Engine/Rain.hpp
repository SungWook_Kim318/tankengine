#pragma once

#include <vector>

#include <glm/glm.hpp>
#include "Engine/Shader.hpp"

#include "GraphicHelperFunc.hpp"

class Rain
{
public:
  constexpr static unsigned nop = 2048;

  Rain(unsigned numberOfParticle = nop);
  ~Rain();

  void Init();
  void Update();
  void Draw() const;
  void Close();

  void SetGlobalVelocity(glm::vec3& velocity);
  const glm::vec3 GetGlobalVelocity(void);

  void SetPhysicsUpdate(bool);
  void SetRegeneration(bool);
  bool IsPhysicsUpdate(void);
  bool IsRegeneration(void);

  static float cutoffIn;
  static float cutoffOut;

  glm::vec2 rainSize;

private:
  const unsigned numParticle;
  glm::vec3 global_Velocity;
  float Size = 10.f;

  bool isUpdate;
  bool isRegeneration;

  Shader* particle;
  Shader* integration;
  Shader* regeneration;

  std::vector<glm::vec4> positionData;
  std::vector<glm::vec4> velocityData;
  std::vector<glm::vec4> DrawPositionData;

  unsigned vao;
  unsigned positions_vbo, velocities_vbo, drawPos_vbo;


  struct Texture
  {
    unsigned textureID;
    int width, height;
    int channel;
  };

  Texture rainTexture;
  //GLuint ssbos[2];
};