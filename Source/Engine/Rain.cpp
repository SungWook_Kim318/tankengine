#include "Rain.hpp"

#include "MyGlWindow.hpp"
#include "global.hpp"

#include "TimeManager.hpp"

//#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/random.hpp>

#include <array>

#include "Externs/stb_image.h"

const static glm::vec4 min{-50.f,-50.f,-50.f, 0.0f};
const static glm::vec4 max{50.f, 50.f, 50.f, 0.0f};

Rain::Rain(unsigned numberOfParticle)
  : numParticle{numberOfParticle}, global_Velocity{0.0f,-1.0f,0.0f},
  particle{nullptr}, integration{nullptr},
  positionData{numberOfParticle}, velocityData{numberOfParticle},
  DrawPositionData{numberOfParticle},
  rainTexture{},
  rainSize{0.005,0.02}
{

}


Rain::~Rain()
{ }


void Rain::Init()
{
  //static_assert(cutoffIn < cutoffOut, "RECOMEND SUICIDED.");
  // ---------------------------------------------------------------------------------
  // Load & compile shaders code
  // ---------------------------------------------------------------------------------
  particle = new Shader{"shaders/rain/particle.vert", "shaders/rain/particle.frag", "shaders/rain/particle.geom"};
  integration = new Shader{"shaders/rain/integrate.comp"};
  regeneration = new Shader{"shaders/rain/regeneration.comp"};

  std::cout << "Number of particle : " << numParticle << '\n';
  // debuging----------------------------------------------------------------------
#ifdef _DEBUG
  auto vec4_to_string = [](const glm::vec4& v) -> std::string
  {
    return std::string{std::to_string(v.x) + ", " + std::to_string(v.y) + ", " + std::to_string(v.z) + ", " + std::to_string(v.w) + '\n'};
  };
  auto vec3_to_string = [](const glm::vec3& v) -> std::string
  {
    return std::string{std::to_string(v.x) + ", " + std::to_string(v.y) + ", " + std::to_string(v.z) + '\n'};
  };
#endif // DEBUG
  // ---------------------------------------------------------------------------------
  // Init Position of particles and set up SSBO
  // ---------------------------------------------------------------------------------

  for(auto i = 0 ; i < numParticle; ++i)
  {
    positionData[i] = glm::vec4(glm::linearRand(min, max));
    positionData[i].w = 1.0f;
    velocityData[i] = {}; //glm::linearRand(glm::vec4{0.0f, 0.0f, 0.0f, 0.0f}, glm::vec4{0.0f, 0.0f, 0.0f, 0.0f});
  }

  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  glGenBuffers(1, &positions_vbo);
  glGenBuffers(1, &velocities_vbo);
  //glGenBuffers(1, &drawPos_vbo);

  // fill with initial data
  glBindBuffer(GL_ARRAY_BUFFER, positions_vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4)*numParticle, &positionData[0], GL_STATIC_DRAW);

  // fill with initial data
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, velocities_vbo);
  glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(glm::vec4)*numParticle, &velocityData[0], GL_STATIC_DRAW);

  // fill with initial data
  //glBindBuffer(GL_ARRAY_BUFFER, drawPos_vbo);
  //glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4)*numParticle, DrawPositionData.data(), GL_STATIC_DRAW);

  // set up generic attrib pointers
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (char*)0 + 0 * sizeof(GLfloat));

  const GLuint ssbos[] = {positions_vbo, velocities_vbo};

  glBindBuffersBase(GL_SHADER_STORAGE_BUFFER, 0, 2, ssbos);

  // ---------------------------------------------------------------------------------
  // Load Image and set texture
  // ---------------------------------------------------------------------------------
  /*
  STBIDEF stbi_uc *stbi_load(char const *filename, int *x, int *y, int *comp, int req_comp)
  //     N=#comp     components
  //       1           grey
  //       2           grey, alpha
  //       3           red, green, blue
  //       4           red, green, blue, alpha
  int texture
  STBIDEF stbi_uc *stbi_load(char const *filename, int *x, int *y, int *comp, int req_comp)
  */

  constexpr char* textuePath = "textures/raintexture.png";
  glGenTextures(1, &rainTexture.textureID);
  glBindTexture(GL_TEXTURE_2D, rainTexture.textureID); // all upcoming GL_TEXTURE_2D operations now have effect on this texture object
  // set the texture wrapping parameters
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);	// set texture wrapping to GL_REPEAT (default wrapping method)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  // set texture filtering parameters
  /*
  GL_NEAREST_MIPMAP_NEAREST
  GL_LINEAR_MIPMAP_NEAREST
  GL_NEAREST_MIPMAP_LINEAR
  GL_LINEAR_MIPMAP_LINEAR
  */
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // load image, create texture and generate mipmaps
  // The FileSystem::getPath(...) is part of the GitHub repository so we can find files on any IDE/platform; replace it with your own image path.
  unsigned char *data = stbi_load(textuePath, &rainTexture.width, &rainTexture.height, &rainTexture.channel, 0);
  if(data)
  {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, rainTexture.width, rainTexture.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
  }
  else
  {
    std::cout << "Failed to load texture" << std::endl;
    throw(std::exception());
  }
  stbi_image_free(data);
}

void Rain::Update()
{
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, positions_vbo);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, velocities_vbo);


  auto& TM = TimeManager::instance();
  integration->use();
  integration->setFloat("dt", static_cast<float>(TM.dt()));
  integration->setVec3("global_Velocity", global_Velocity);
  if(isUpdate)
    glDispatchCompute(numParticle / 256, 1, 1);


  regeneration->use();
  regeneration->setVec4("minimum", min);
  regeneration->setVec4("maximum", max);
  
  if(isRegeneration)
    glDispatchCompute(numParticle / 256, 1, 1);
}

void Rain::Draw() const
{
  const float ratio = global::win->getWindowRatio();
  const float fov = global::win->m_viewer->getAspectRatio();
  const float znear = global::win->getNear();
  const float zfar = global::win->getFar();

  glm::vec3 eye = global::win->m_viewer->getViewPoint(); // m_viewer->getViewPoint().x(), m_viewer->getViewPoint().y(), m_viewer->getViewPoint().z());
  glm::vec3 look = global::win->m_viewer->getViewCenter();   //(m_viewer->getViewCenter().x(), m_viewer->getViewCenter().y(), m_viewer->getViewCenter().z());
  glm::vec3 up = global::win->m_viewer->getUpVector(); // m_viewer->getUpVector().x(), m_viewer->getUpVector().y(), m_viewer->getUpVector().z());

  glm::mat4 Projection = glm::perspective(ratio, fov, znear, zfar);
  glm::mat4 View = lookAt(eye, look, up);

  particle->use();


  //const glm::vec3 eye = global::win->m_viewer->getViewPoint();
  glm::vec4 velo{eye + global_Velocity, 1.0f};
  velo = View * velo;
  velo.y > 0.0f ? velo.y = -velo.y : velo.y;
  // set the uniforms
  particle->setMat4("View", View);
  particle->setMat4("Projection", Projection);
  particle->setVec4("velocity", velo);
  particle->setVec2("particleSize", rainSize);


  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, rainTexture.textureID);
  // bind the current vao
  glBindVertexArray(vao);

  // we are blending so no depth testing
  //glDisable(GL_DEPTH_TEST);
  // enable blending
  glDisable(GL_CULL_FACE);
  glEnable(GL_BLEND);
  //  and set the blend function to result = 1*source + 1*destination
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glDrawArrays(GL_POINTS, 0, numParticle);

  // Set back
  //glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);
  glEnable(GL_CULL_FACE);
}

void Rain::Close()
{ }

void Rain::SetGlobalVelocity(glm::vec3& velocity)
{
  global_Velocity = velocity;
}

const glm::vec3 Rain::GetGlobalVelocity(void)
{
  return global_Velocity;
}

void Rain::SetPhysicsUpdate(bool b)
{
  isUpdate = b;
}

void Rain::SetRegeneration(bool b)
{
  isRegeneration = b;
}

bool Rain::IsPhysicsUpdate(void)
{
  return isUpdate;
}

bool Rain::IsRegeneration(void)
{
  return isRegeneration;
}
