#include "CallbackFunctions.hpp"

#include "global.hpp"

static bool lbutton_down;
static bool rbutton_down;
static bool mbutton_down;
static double m_lastMouseX;
static double m_lastMouseY;
static double cx, cy;

void window_size_callback(GLFWwindow* window, int width, int height)
{
  global::win->setSize(width, height);
  global::win->setAspect(width / (float)height);

}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GLFW_TRUE);
  /*
  if(key == GLFW_KEY_R && action == GLFW_PRESS)
    global::win->stopSnowRegeration = !global::win->stopSnowRegeration;
  if(key == GLFW_KEY_P && action == GLFW_PRESS)
    global::win->stopSnowUpdate = !global::win->stopSnowUpdate;
    */
}


void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos)
{
  cx = xpos;
  cy = ypos;
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
  if(action == GLFW_PRESS) {
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    m_lastMouseX = xpos;
    m_lastMouseY = ypos;
  }

  if(button == GLFW_MOUSE_BUTTON_LEFT) {
    if(GLFW_PRESS == action)
      lbutton_down = true;
    else if(GLFW_RELEASE == action)
      lbutton_down = false;
  }

  else if(button == GLFW_MOUSE_BUTTON_RIGHT) {
    if(GLFW_PRESS == action)
      rbutton_down = true;
    else if(GLFW_RELEASE == action)
      rbutton_down = false;
  }

  else if(button == GLFW_MOUSE_BUTTON_MIDDLE) {
    if(GLFW_PRESS == action)
      mbutton_down = true;
    else if(GLFW_RELEASE == action)
      mbutton_down = false;
  }
}


void mouseDragging(double width, double height)
{


  if(lbutton_down) {
    float fractionChangeX = static_cast<float>(cx - m_lastMouseX) / static_cast<float>(width);
    float fractionChangeY = static_cast<float>(m_lastMouseY - cy) / static_cast<float>(height);
    global::win->m_viewer->rotate(fractionChangeX, fractionChangeY);
  }
  else if(mbutton_down) {
    float fractionChangeX = static_cast<float>(cx - m_lastMouseX) / static_cast<float>(width);
    float fractionChangeY = static_cast<float>(m_lastMouseY - cy) / static_cast<float>(height);
    global::win->m_viewer->zoom(fractionChangeY);
  }
  else if(rbutton_down) {
    float fractionChangeX = static_cast<float>(cx - m_lastMouseX) / static_cast<float>(width);
    float fractionChangeY = static_cast<float>(m_lastMouseY - cy) / static_cast<float>(height);
    global::win->m_viewer->translate(-fractionChangeX, -fractionChangeY, 1);
  }


  m_lastMouseX = cx;
  m_lastMouseY = cy;

}