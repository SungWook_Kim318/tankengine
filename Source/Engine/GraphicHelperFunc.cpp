#include "GraphicHelperFunc.hpp"

#include <gl\glew.h>

#include <iostream>
#include <cstdio>


#define STB_IMAGE_IMPLEMENTATION
#include "Externs/stb_image.h"
 

namespace GraphicHelper
{
  // ---------------------------------------------------------------------------------
  // utility function for loading a 2D texture from file
  // Load Image and set texture
  // ---------------------------------------------------------------------------------

  unsigned loadTexture(const char *path, bool gammaCorrection)
  {
    unsigned int textureID;
    glGenTextures(1, &textureID);

    int width, height, nrComponents;
    /*
    STBIDEF stbi_uc *stbi_load(char const *filename, int *x, int *y, int *comp, int req_comp)
    //     N=#comp     components
    //       1           grey
    //       2           grey, alpha
    //       3           red, green, blue
    //       4           red, green, blue, alpha
    int texture
    STBIDEF stbi_uc *stbi_load(char const *filename, int *x, int *y, int *comp, int req_comp)
    */
    unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
    if(data)
    {
      GLenum internalFormat = 0;
      GLenum dataFormat = 0;
      if(nrComponents == 1)
      {
        internalFormat = dataFormat = GL_RED;
      }
      else if(nrComponents == 3)
      {
        internalFormat = gammaCorrection ? GL_SRGB : GL_RGB;
        dataFormat = GL_RGB;
      }
      else if(nrComponents == 4)
      {
        internalFormat = gammaCorrection ? GL_SRGB_ALPHA : GL_RGBA;
        dataFormat = GL_RGBA;
      }

      glBindTexture(GL_TEXTURE_2D, textureID);
      glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, dataFormat, GL_UNSIGNED_BYTE, data);
      glGenerateMipmap(GL_TEXTURE_2D);

      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

      stbi_image_free(data);
    }
    else
    {
      std::cout << "Texture failed to load at path: " << path << std::endl;
      stbi_image_free(data);
    }

    return textureID;
  }
}