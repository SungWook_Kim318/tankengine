//#define  FREEGLUT_LIB_PRAGMAS  0
#pragma once

#pragma warning(push)
#pragma warning(disable:4311)		// convert void* to long
#pragma warning(disable:4312)		// convert long to void*


#include <windows.h>
#include <iostream>


#include "GL/glew.h"

#include <string>




#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/matrix_inverse.hpp"

#include <chrono>

#include "Objects/cube.hpp"
#include "Engine/Viewer.hpp"
#include "Engine/Model.hpp"
#include "Engine/Screen.hpp"
#include "Engine/Mesh.hpp"
#include "Engine/Shader.hpp"

#include "Engine/Snow.hpp"
#include "Engine/Rain.hpp"
#pragma warning(pop)


class MyGlWindow
{
public:
  MyGlWindow(int w, int h);
  ~MyGlWindow();
  void draw();
  void update();
  void init();
  void setSize(int w, int h)
  {
    m_width = w; m_height = h;
  }
  void setAspect(float r)
  {
    m_viewer->setAspectRatio(r);
  }
  void initialize();

  float getWindowRatio(void)
  {
    return static_cast<float>(m_height) / m_width;
  }
  float getFar(void)
  {
    return z_far;
  }
  float getNear(void)
  {
    return z_near;
  }
  Viewer *m_viewer;

private:

  float z_near;
  float z_far;
  int m_width;
  int m_height;

  //glm::vec2 _time = glm::vec2(0.0f, 0.0f);

  void drawMesh(glm::mat4 & view, glm::mat4 & projection);

  Model m_model;
  Snow m_snow;
  Rain m_rain;
  
  bool updateSnow = false;
  bool updateRain = true;
};