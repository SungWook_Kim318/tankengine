#include "global.hpp"
#include "MyGlWindow.hpp"

struct SpotLight;

namespace global
{
  glm::vec3 lightPos = glm::vec3(10, 10, 10);
  glm::vec3 lightIntensity{1.0,1.0,1.0};
  glm::vec3 backGround = glm::vec3(0.1);
	
  MyGlWindow * win;
  SpotLight spotlight;
}
