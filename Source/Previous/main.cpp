#include <GL\glew.h>
#include <GLFW/glfw3.h>

 //glm is used to create perspective and transform matrices
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/random.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <cmath>


#define STB_IMAGE_IMPLEMENTATION
#include "../extern/stb_image.h"

// Custom Libs
#include "Shader.hpp"
#include "Model.hpp"
#include "Camera.hpp"

#include "../extern/imgui/imgui.h"
#include "../extern/IMGUI/imgui_impl_glfw.h"
#include "../extern/IMGUI/imgui_impl_opengl3.h"

static unsigned maxFPS, minFPS, avgFPS;

namespace {
  Camera camera{glm::vec3{-15.0f, 0.0f, 15.0f}, glm::vec3{0.0f, 1.0f, 0.0f}, 0, 0};
  unsigned width = 1280;
  unsigned height = 720;
  constexpr unsigned particles = 1024;

  // timing
  double dt = 1/60.0f;  // time between current frame and last frame
  double lastFrame = 0.0f;

  glm::vec4 backgroundColor{0.0};
}

namespace snowSetting
{
  glm::vec3 global_Velocity{0.0, -1.0, 0.0};
  float Size = 10.f;
}

//Call Back Functions
void Keyboard_CB(GLFWwindow *window);
void Mouse_Button_CB(GLFWwindow* window, int button, int action, int mods);
void MouseScroll_CB(GLFWwindow* window, double xoffset, double yoffset);
void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos);
int main()
{
  if(!glfwInit()) {
    std::cerr << "failed to init GLFW" << std::endl;
    return 1;
  }

  // select opengl version
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

  // create a window
  GLFWwindow *window;
  if((window = glfwCreateWindow(width, height, "13compute_shader_nbody", 0, 0)) == 0) {
    std::cerr << "failed to open window" << std::endl;
    glfwTerminate();
    return 1;
  }

  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO();
  (void)io;

  io.Fonts->AddFontFromFileTTF("D2Coding.ttf", 14.f, NULL, io.Fonts->GetGlyphRangesKorean());

  const char glsl_version[] = "#version 450";
  ImGui_ImplGlfw_InitForOpenGL(window, true);
  ImGui_ImplOpenGL3_Init(glsl_version);

  ImGui::StyleColorsDark();
  
  glfwMakeContextCurrent(window);

  // tell GLFW to capture our mouse
  glfwSetCursorPosCallback(window, cursor_pos_callback);
  glfwSetMouseButtonCallback(window, Mouse_Button_CB);
  glfwSetScrollCallback(window, MouseScroll_CB);

  //Init Glew
  glewExperimental = true; // Needed for core profile
  if(glewInit()) {
    std::cerr << "failed to init GLEW" << std::endl;
    glfwDestroyWindow(window);
    glfwTerminate();
    return 1;
  }

  glfwSwapInterval(false);

  // shader source code
  Shader particle{"particle.vert","particle.frag","particle.geom"};
  //Shader acceleration{"acceleration.comp"};
  //Shader tiledAcceleration{"tiled_acceleration.comp"};
  Shader integralation{"integrate.comp"};
  

  std::cout << "Number of particle : " << particles << '\n';
  // randomly place particles in a cube
  std::vector<glm::vec4> positionData(particles);
  std::vector<glm::vec4> velocityData(particles);

  glm::mat4 inverseProj{1.0};
  glm::mat4 inverseView{1.0};
  
  {
    const float ratio = static_cast<float>(height) / width;
    glm::mat4 Projection = glm::perspective(glm::radians(camera.Zoom), ratio, 10.f, 50.0f);
    glm::mat4 View = camera.GetViewMatrix();
    inverseProj = glm::inverse(Projection);
    inverseView = glm::inverse(View);
  }

  for(int i = 0; i < particles; ++i) {
    //Z - value use different Random generation
    // initial position
    auto gen = glm::linearRand(glm::vec4(-2, -2, 0.735758, 1), glm::vec4(2, 2, 7.389056, 1));//z = [2/e,e^2]
    auto origin = glm::vec4{gen.x, gen.y, glm::log(gen.z)/2, 1.0f};

    auto position = inverseProj * origin;
    position = inverseView * position;
    position /= position.w > 0.f ? position.w : -position.w;
    position.w = 1.0;
    positionData[i] = position;
    velocityData[i] = glm::linearRand(glm::vec4(-0.1, -0.2, -0.1, 0), glm::vec4(0.1, -0.1, 0.1, glm::two_pi<float>()));
  }

  // generate positions_vbos and vaos
  GLuint vao, positions_vbo, velocities_vbo;

  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  glGenBuffers(1, &positions_vbo);
  glGenBuffers(1, &velocities_vbo);

  glBindBuffer(GL_SHADER_STORAGE_BUFFER, velocities_vbo);
  glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(glm::vec4)*particles, &velocityData[0], GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, positions_vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4)*particles, &positionData[0], GL_STATIC_DRAW);

  // set up generic attrib pointers
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (char*)0 + 0 * sizeof(GLfloat));

  const GLuint ssbos[] = {positions_vbo, velocities_vbo};
  glBindBuffersBase(GL_SHADER_STORAGE_BUFFER, 0, 2, ssbos);

  

  // we are blending so no depth testing
  glDisable(GL_DEPTH_TEST);

  // enable blending
  glEnable(GL_BLEND);
  //  and set the blend function to result = 1*source + 1*destination
  glBlendFunc(GL_ONE, GL_ONE);


  // Main Loop
  while(!glfwWindowShouldClose(window)) {
    // per-frame time logic
    // --------------------
    double currentFrame = glfwGetTime();
    dt = currentFrame - lastFrame;
    lastFrame = currentFrame;
    /////////// ImGUI
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    glfwPollEvents();

    Keyboard_CB(window);

    if(ImGui::Begin(u8"������"))
    {
      ImGui::SetWindowFontScale(1);
      ImGui::ColorEdit3("BackGround Color", &::backgroundColor[0]);
      ImGui::InputFloat4("Camera Position", &camera.Position[0]);
      ImGui::SliderFloat("Camera Yaw", &camera.Yaw,-360,360);
      ImGui::SliderFloat("Camera Pitch", &camera.Pitch,-90, 90);

      ImGui::SliderFloat("Camera Moving Speed", &camera.MovementSpeed, 0, 50,"%0.6f");
      ImGui::SliderFloat("Camera Rotation Speed", &camera.MouseSensitivity, 0, 1, "%0.6f");

      ImGui::SliderFloat3("global: Snow Speed", &snowSetting::global_Velocity[0], -5.0, 5.0);
      ImGui::VSliderFloat("global: Snow Fallin Speed", {10,100}, &snowSetting::global_Velocity.y, -10, 0);

      float fps = 1/dt;
      ImGui::InputFloat("FPS", &fps, 0.f, 0.f, 10);
      
    }ImGui::End();

    // Do compute shader
    integralation.use();
    // setup uniforms
    integralation.setFloat("dt", static_cast<float>(::dt));
    integralation.setVec3("global_Velocity", snowSetting::global_Velocity);

    // Do compute shader
    glDispatchCompute(particles / 256, 1, 1);

    // clear first
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(::backgroundColor.r,
                 ::backgroundColor.g,
                 ::backgroundColor.b,
                 ::backgroundColor.a);
    // use the shader program
    particle.use();

    // calculate ViewProjection matrix
    const float ratio = static_cast<float>(height) / width;
    
    glm::mat4 Projection = glm::perspective(glm::radians(camera.Zoom), ratio, 0.1f, 500.0f);
    glm::mat4 View = camera.GetViewMatrix();
    
    // set the uniforms
    particle.setMat4("View", View);
    particle.setMat4("Projection", Projection);

    // bind the current vao
    glBindVertexArray(vao);

    // draw
    glDrawArrays(GL_POINTS, 0, particles);
    // check for errors
    GLenum error = glGetError();
    if(error != GL_NO_ERROR) {
      std::cerr << error << std::endl;
      break;
    }

    {
      static bool isFirst = true;
      unsigned fps = 1.0 / dt;
      
      if(maxFPS < fps)
        maxFPS = fps;
      if(minFPS > fps && fps >= 2)
        minFPS = fps;
      avgFPS = (avgFPS + fps) / 2;

      if(isFirst)
      {
        maxFPS = minFPS = avgFPS = fps;
        minFPS = 100000;
        isFirst = false;
      }
    }
    
    ///////// Render ImGUI
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    // finally swap buffers
    glfwSwapBuffers(window);
  }

  std::cout << "Max : " << maxFPS << " Frame / second" << std::endl;
  std::cout << "Min : " << minFPS << " Frame / second" << std::endl;
  std::cout << "AVG : " << avgFPS << " Frame / second" << std::endl;

  // delete the created objects

  glDeleteVertexArrays(1, &vao);
  glDeleteBuffers(1, &positions_vbo);
  glDeleteBuffers(1, &velocities_vbo);

  ///////// Delete ImGui
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();

  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
}

void Keyboard_CB(GLFWwindow *window)
{
  if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(window, true);

  if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    camera.ProcessKeyboard(FORWARD, dt);
  if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    camera.ProcessKeyboard(BACKWARD, dt);
  if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    camera.ProcessKeyboard(LEFT, dt);
  if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    camera.ProcessKeyboard(RIGHT, dt);
}

static bool lbutton_down;
static bool rbutton_down;
static bool mbutton_down;
static bool first = true;
static double lastX;
static double lastY;

static void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos)
{
  if(lbutton_down)
  {
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    if(first)
    {
      lastX = xpos;
      lastY = ypos;
      first = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = ypos - lastY;

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
  }
}

void Mouse_Button_CB(GLFWwindow* window, int button, int action, int mods)
{
  if(action == GLFW_PRESS) {
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    lastX = xpos;
    lastY = ypos;
  }

  if(button == GLFW_MOUSE_BUTTON_LEFT) {
    if(GLFW_PRESS == action)
      lbutton_down = true;
    else if(GLFW_RELEASE == action)
      lbutton_down = false;
  }

  else if(button == GLFW_MOUSE_BUTTON_RIGHT) {
    if(GLFW_PRESS == action)
      rbutton_down = true;
    else if(GLFW_RELEASE == action)
      rbutton_down = false;
  }

  else if(button == GLFW_MOUSE_BUTTON_MIDDLE) {
    if(GLFW_PRESS == action)
      mbutton_down = true;
    else if(GLFW_RELEASE == action)
      mbutton_down = false;
  }
}
void MouseScroll_CB(GLFWwindow* window, double xoffset, double yoffset)
{
  camera.ProcessMouseScroll(yoffset);
}